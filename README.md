# Existing wordpress project walkthrough

## Dependencies
### 1. Vagrant Installation
- Download installation package [here](https://www.vagrantup.com/downloads.html).
- Run the installation package.

### 2. Virtualbox 
- Download installation package [here](https://www.virtualbox.org/wiki/Downloads).
- Run the installation package
- If there are permission errors during installation, follow this [guide](https://github.com/docksal/docksal/issues/417).

## Clone Repository
Clone the repository on bitbucket. Open terminal and run the ff. commands.
```
$ cd path/to/development/directory/ # Your preferred path
$ git clone https://kingracosalem-pistacia@bitbucket.org/kingracosalem-pistacia/wp_proj.git
```

## Project Setup (Local environment)
### 1. Clone ccc-website project for demonstration.
Note : If the git clone command below fails, kindly copy the link on this repo link : https://bitbucket.org/pistacia/ccc-website/src/master/
```
$ cd wp_project
$ git clone git@bitbucket.org:pistacia/ccc-website.git
```
### 2. Run vagrant development environment
```
$ vagrant up
```

### 3. Add your guest machines public key to vagrant.
```
$ nano ~/.ssh/id_rsa.pub # Copy this key
$ vagrant ssh
$ nano .ssh/authorized_keys # Add your guest machines public key here and save
```

### 4. Stop vagrant SSH session and transfer setup files for the project.
### **(Note: wp_proj_setup.sh is only configured for ccc-website repo only)**
```
$ exit
$ scp src/config_files/local/default vagrant@192.168.33.11:/tmp
$ scp src/scripts/local/lemp_setup.sh vagrant@192.168.33.11:/tmp
$ scp src/scripts/local/wp_proj_setup.sh vagrant@192.168.33.11:/tmp
```

### 5. Setup LEMP stack for existing wordpress project.
```
$ vagrant ssh
$ bash /tmp/lemp_setup.sh
```

### 6. Setup exiting wordpress project
```
$ bash /tmp/wp_proj_setup.sh
```

### 7. Wordpress project access link.
```
192.168.33.11
```

## For testing
### 1. Create wordpress admin
```
$ vagrant ssh
$ cd /home/vagrant/sync/ccc_hp
$ wp user create pistacia test@email.com --role=administrator # Copy password and store it somewhere
```

### 2. Duplicate an existing page
#### - Login to this url using the wordpress admin in step #1
```
http://192.168.33.11/wp-admin/
```
- Go to page section
```
http://192.168.33.11/wp-admin/edit.php?post_type=page
```
#### - Hover over an item in page table.
```
Click 'Duplicate This'
```