#!/bin/bash
# ------------------------
# Wordpress Admin vars
# ------------------------
WP_ADMIN=pistacia
WP_ADMIN_PW=password
WP_ADMIN_EMAIL=admin@email.com

URL=192.168.33.11
TITLE=ccc website
BLOG_DESC=test

DB_NAME=cccjph
DB_USER=cccjph

echo "-> Installing Wordpress CLI ..."
cd /tmp
sudo curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
sudo chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

# ------------------------------
# Wordpress installation
# ------------------------------
echo "-> Installing Wordpress ..."
cd /home/vagrant/sync/ccc_hp
wp core download
wp core config --dbname=$DB_NAME --dbuser=$DB_USER --dbpass=password --dbhost=localhost --dbprefix=wp_
wp core install --url="${URL}" --title="${TITLE}" --admin_user=$WP_ADMIN --admin_password=$WP_ADMIN_PW --admin_email=$WP_ADMIN_EMAIL
wp option update blogdescription "${BLOG_DESC}"

# --------------------------------
# Wordpress project configuration
# --------------------------------
echo "-> Configuring wordpress project ..."
wp theme install /home/vagrant/sync/ccc_hp/themes/ccc.zip --activate
wp plugin install contact-form-7 wp-mail-smtp wp-multibyte-patch wordpress-importer duplicate-page --activate
wp db import /home/vagrant/sync/ccc_hp/data/cccjph-dbc38d4.sql

echo "-> Restarting all services ..."
sudo systemctl restart nginx