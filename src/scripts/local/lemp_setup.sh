#!/bin/bash
DB_NAME=cccjph
DB_ROOT=root
DB_ROOT_PW=password
DB_USER=cccjph
DB_PW=password

echo "-----------------------------------------"
echo "LEMP stack installation and configuration"
echo "-----------------------------------------"

echo "-> Configuring locale ..."
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

echo "-> Updating Ubuntu 16.04 ..."
sudo apt-get update

echo "-> Installing Nginx ..."
sudo apt-get install nginx -y &>/dev/null

echo "-> Installing Git ..."
sudo apt-get install git -y &>/dev/null

echo "-> Configuring UFW firewall ..."
sudo ufw allow 'NGINX HTTP'
sudo ufw allow 'OpenSSH'
sudo mv /tmp/default /etc/nginx/sites-available/default

echo "-> MySQL root password initial setup ..."
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $DB_ROOT_PW"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DB_ROOT_PW"

echo "-> Installing MySQL"
sudo apt-get install mysql-server -y &>/dev/null

echo "-> Configuring database"
mysql -u$DB_ROOT -p$DB_ROOT_PW -e "CREATE DATABASE $DB_NAME DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;" &>/dev/null
mysql -u$DB_ROOT -p$DB_ROOT_PW -e "GRANT ALL ON $DB_NAME.* TO '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PW';" &>/dev/null
mysql -u$DB_ROOT -p$DB_ROOT_PW -e "FLUSH PRIVILEGES;" &>/dev/null

echo "-> Installing PHP and additional Extensions ..."
sudo apt-get install php-fpm -y &>/dev/null
sudo apt-get install php-mysql -y &>/dev/null
sudo apt-get install php-curl -y &>/dev/null
sudo apt-get install php-gd -y &>/dev/null
sudo apt-get install php-mbstring -y &>/dev/null
sudo apt-get install php-mcrypt -y &>/dev/null
sudo apt-get install php-xml -y &>/dev/null
sudo apt-get install php-xmlrpc -y &>/dev/null

# ------------------------------
# This basically would allow users to craft PHP requests
# in a way that would allow them to execute scripts that
# they shoudln't be allowed to execute.
#
# reference : https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-in-ubuntu-16-04
# document location : Step 3
# ------------------------------
sudo sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' /etc/php/7.0/fpm/php.ini

echo "-> Restarting PHP processor ..."
sudo systemctl restart php7.0-fpm